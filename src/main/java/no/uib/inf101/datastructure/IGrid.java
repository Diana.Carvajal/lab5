package no.uib.inf101.datastructure;

import java.awt.Color;

/**
 * Represents a generic grid structure.
 *
 * @param <T> the type of elements stored in the grid
 */
public interface IGrid<T> extends GridDimension, GridCellCollection<T> {

  /**
   * Get the element at the given position.
   *
   * @param pos the position
   * @return the position of the cell
   * @throws IndexOutOfBoundsException if the position is out of bounds
   */
  T get(CellPosition pos);

  /**
   * Set the element at the given position in the grid.
   *
   * @param pos the position
   * @param elem the new element
   * @throws IndexOutOfBoundsException if the position is out of bounds
   */
  void set(CellPosition pos, T elem);

}